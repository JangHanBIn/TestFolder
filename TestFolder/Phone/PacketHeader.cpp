#include "PacketHeader.h"
#include <arpa/inet.h>
#include "errhandling.h"
#include <netdb.h>
#include <iostream>
#include <cstdlib>
#include <ctime>



PacketHeader::PacketHeader()
{
    memset(this,0,sizeof(PacketHeader));


    //set ipheader
    iph.version=4;
    iph.ihl=5;
    iph.frag_off=htons(IP_DF); //don't flagment set
    iph.ttl=64;
    iph.protocol=IPPROTO_TCP;

    //set tcpheader
    tcph.doff=5;
    tcph.window=htons(2048);

}

void PacketHeader::init(uint32_t sAddr, uint32_t dAddr, uint16_t sPort, uint16_t dPort)
{
    iph.saddr=sAddr;
    iph.daddr=dAddr;
    tcph.source=htons(sPort);
    tcph.dest=htons(dPort);
}

void PacketHeader::incId()
{
    if(ntohs(iph.id)!=65535)
        iph.id+=htons(1);
    else
        iph.id=0;
}

void PacketHeader::incSeq()
{
    if((ntohl(tcph.seq)!=4294967295))
        tcph.seq+=htonl(1);
    else
        tcph.seq=0;
}

void PacketHeader::incAck()
{
    if((ntohl(tcph.ack_seq)!=4294967295))
        tcph.ack_seq+=htonl(1);
    else
        tcph.ack_seq=0;
}

void PacketHeader::makeId()
{
    srand((unsigned int)time(NULL));
    iph.id=htons(rand()%65535);
}

void PacketHeader::makeSeq()
{
    srand((unsigned int)time(NULL));
    tcph.seq=htonl(rand()%4294967295);
}

void PacketHeader::makeAck()
{
    srand((unsigned int)time(NULL));
    tcph.ack_seq=htonl(rand()%4294967295);
}

void PacketHeader::rstFlags()
{
    tcph.res1=0;
    tcph.fin=0;
    tcph.syn=0;
    tcph.rst=0;
    tcph.psh=0;
    tcph.ack=0;
    tcph.urg=0;
    tcph.res2=0;
}


