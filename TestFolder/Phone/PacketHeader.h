#ifndef PACKETHEADER_H
#define PACKETHEADER_H
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinfo.h>

#pragma pack(push,1)
class PacketHeader
{

public:
    PacketHeader();
    struct iphdr iph;
    struct tcphdr tcph;
    void init(uint32_t sAddr,uint32_t dAddr,uint16_t sPort,uint16_t dPort);
    void makeId();
    void makeSeq();
    void makeAck();
    void rstFlags();
    void incId();
    void incSeq();
    void incAck();

};
#pragma pack(pop)
#endif // PACKETHEADER_H
