TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
LIBS +=-lpcap
LIBS +=-pthread
SOURCES += main.cpp \
    calchecksum.cpp \
    jpcaplib.cpp \
    netinfo.cpp \
    printdata.cpp \
    errhandling.cpp \
    tuntap.cpp \
    packetsend.cpp \
    PacketHeader.cpp \
    tcpconnection.cpp \
    jrawlib.cpp

DISTFILES += \
    Phone.pro.user

HEADERS += \
    calchecksum.h \
    jpcaplib.h \
    netinfo.h \
    printdata.h \
    errhandling.h \
    tuntap.h \
    packetsend.h \
    PacketHeader.h \
    tcpconnection.h \
    jrawlib.h
