#include "errhandling.h"
#include <iostream>
#include <cstring>

void errorRetn(std::string content)
{
    perror(content.c_str());
    exit(1);

}
