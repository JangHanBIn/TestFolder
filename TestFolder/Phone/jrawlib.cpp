#include "jrawlib.h"

int rOpen()
{
    int rawFd;
    if((rawFd=socket(AF_INET,SOCK_RAW,IPPROTO_RAW))<0)
        errorRetn("Socket Open error in recvPacket ");

    return rawFd;
}

int rOpen(int type)
{
    int rawFd;
    if((rawFd=socket(AF_INET,SOCK_RAW,type))<0)
        errorRetn("Socket Open error in recvPacket ");

    return rawFd;
}


bool rRecvPacket(int rawFd, uint8_t* recvPacketBuffer,int packetSize,int& recvPacketLen)
{


    if((recvPacketLen=recv(rawFd,recvPacketBuffer,packetSize,0))<0)
        errorRetn("Recv Packet Error");

    return true;
}
