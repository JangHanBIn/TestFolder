#ifndef JRAWLIB_H
#define JRAWLIB_H
#include <netinet/ip.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "errhandling.h"

int rOpen();
int rOpen(int type);
int rSendPacket();
bool rRecvPacket(int rawFd, uint8_t *recvPacketBuffer, int packetSize, int &recvPacketLen);

#endif // JRAWLIB_H
