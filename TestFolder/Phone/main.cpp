#include <iostream>
#include <cstring>
#include "netinfo.h"
#include "tuntap.h"
#include "route_set.h"
#include "jpcaplib.h"
#include "tcpconnection.h"
#include "printdata.h"
#include "packetsend.h"
#include <unistd.h>
#include "jrawlib.h"
#include <map>
#include <netinet/udp.h>
#include <netinet/ip_icmp.h>
#include <thread>

using namespace std;


struct MapKey{
    uint32_t destIP;
    uint16_t srcPort;
    uint16_t destPort;
    uint8_t protocol;
    uint16_t id; //for icmp packet

    bool operator<(const MapKey& other) const
    {
        return tie(destIP,srcPort,destPort,protocol,id)<tie(other.destIP,other.srcPort,other.destPort,other.protocol,other.id);

        /*
         if(this->destIP<other.destIP) return true;
         if(this->srcPort<other.srcPort) return true;
         if(this->destPort<other.destPort) return true;
         if(this->protocol<other.protocol) return true;
         if(this->id<other.id) return true;
         return false;
         */
    }

    void printInfo()
    {
        cout<<"Dest IP : "<<destIP<<endl;
        cout<<"Src Port : "<<srcPort<<endl;
        cout<<"Dest Port : "<<destPort<<endl;
        cout<<"Protocol : "<<(int)protocol<<endl;
        cout<<"id : "<<id<<endl;
    }
    friend ostream& operator << (ostream& os, const MapKey& m)
    {
        return os<<m.destIP ;
    }
};

bool decapPacket(uint8_t** packet, int& dataLen, MapKey &mapKey, SNetInfo& netInfo, map<MapKey, uint32_t> &relayInfo, map<struct MapKey, uint32_t>::iterator& iter);
bool decapPacket(uint8_t **packet, int &dataLen, CNetInfo &netInfo);
bool packetRelay(int rawFd, uint8_t* packet, int &dataLen, uint32_t local_IP, MapKey &mapKey,map<struct MapKey, uint32_t>::iterator& iter, SNetInfo &netInfo, map<MapKey, uint32_t> &relayInfo, PacketHeader &packetHeader);
void forwardPacket(int rawFd,uint8_t** packet,int dataLen,CNetInfo& netInfo);
void relayFunc(int rawFd, int recvRawFd, uint8_t *recvPacketBuffer, int sizeOfBuf, int recvPacketLen, uint8_t *netInfoP);
bool checkPacket(uint8_t **packet, int dataLen, uint32_t srcIP);
#define HOST 0xffffffff

void usage()
{
    cout<<"Usage : ./Phone <Procedural>"<<endl;
    cout<<"Procedural : <Server> or <Client>"<<endl;
    exit(1);
}

int serverProc();
int clientProc();

int main(int argc, char* argv[])
{
    if(argc!=2)
        usage();

    int ret;
    if(strcmp(argv[1],"Server")==0)
        ret=serverProc();
    else if(strcmp(argv[1],"Client")==0)
        ret=clientProc();
    else
        usage();

    return ret;
}

int serverProc()
{
    /*Phone IP is init after tcp connection*/

    SNetInfo netInfo;


    //get network Infomation to routing set
    netInfo.getNetInfo();


  //  strcpy(netInfo.tunDev,"tun0");              //set tunnel interface name
  //  tun_alloc(netInfo.tunDev,IFF_TUN); //allocate TUN interface & retn fd

    //strcpy(netInfo.tunIP,"10.1.1.128");         //set tunnel IP addr

    //set ip address & up & running
//    tun_set(netInfo.tunDev,netInfo.tunIP);



    //get local IP & cpy
    strcpy(netInfo.localIP,getLocalIP(netInfo.localDev));   //getLocalIP is use ioctl

    //get Gateway IP & dev
    netInfo.getGatewayIface();

    PacketHeader packetHeader;

//    pcap_t * pcd=pOpen(netInfo.tunDev);     //open libpcap discrptor use to recv packet
    pcap_t * localPcd=pOpen(netInfo.localDev);
    uint8_t* packet;
    int dataLen=0;

    //do tcp connection
    int rawFd=rOpen(); //Open raw File discrptor use to send packet
//    int socketFlag;
//    if((setsockopt(rawFd,SOL_IP,IP_HDRINCL,&socketFlag,sizeof(int)))==-1)
//    cout<<"Seco Socket Opt Error!"<<endl;
    struct MapKey mapKey;
    map<struct MapKey,uint32_t> relayInfo;
    map<struct MapKey, uint32_t>::iterator iter;

    if(connectClient(localPcd,rawFd,netInfo,packetHeader))//do tcp connection for router & save Info at packetHeader
        while(recvPacket(localPcd,&packet,dataLen))
            packetRelay(rawFd,packet,dataLen,netInfo.local_IP,mapKey,iter,netInfo,relayInfo,packetHeader);



    return 0;
}

int clientProc()
{
    CNetInfo netInfo;

    //get network Infomation to routing set
    netInfo.getNetInfo();


//    //get network Infomation to routing set
//    netInfo.getNetInfo();

    //get local IP & cpy
    strcpy(netInfo.localIP,getLocalIP(netInfo.localDev));   //getLocalIP is use ioctl

    //get proxy IP & cpy
    netInfo.getIpFromDomain();


    //get Gateway IP & dev
    netInfo.getGatewayIface();

    strcpy(netInfo.tunDev,"tun0");              //set tunnel interface name
    tun_alloc(netInfo.tunDev,IFF_TUN); //allocate TUN interface & retn fd

    strcpy(netInfo.tunIP,"10.1.1.128");         //set tunnel IP addr

    //set ip address & up & running
    tun_set(netInfo.tunDev,netInfo.tunIP);

    //converting char to binary
    netInfo.init();

    //print netInfo
    netInfo.printInfo();


    system(netInfo.executeRouteSet.c_str());




    /*Packet Handle*/
    PacketHeader packetHeader;

    pcap_t * pcd=pOpen(netInfo.tunDev);     //open libpcap discrptor use to recv packet
//    pcap_t * localpcd=pOpen(netInfo.localDev);
    uint8_t* packet;
    int dataLen=0;

    //do tcp connection

    uint8_t recvPacketBuffer[2000];
    memset(recvPacketBuffer,0,sizeof(recvPacketBuffer));
    int recvPacketLen=0;

//    int socketFlag;

    int rawFd=rOpen(); //Open raw File discrptor use to send packet
//    if((setsockopt(rawFd,SOL_IP,IP_HDRINCL,&socketFlag,sizeof(int)))==-1)
//        cout<<"Seco Socket Opt Error!"<<endl;
    int recvRawFd=rOpen(IPPROTO_TCP);
    thread t1(relayFunc,rawFd,recvRawFd,recvPacketBuffer,sizeof(recvPacketBuffer),recvPacketLen,(uint8_t*)&netInfo);
    if(connectServer(rawFd,netInfo,packetHeader)) //do tcp connection for router & save Info at packetHeader
        while (true)
        {

            if(recvPacket(pcd,&packet,dataLen)) //recv packet & save point to "packet" & save data len
            {
               struct iphdr* iph=(struct iphdr*)packet;
               if((iph->daddr!=netInfo.tun_IP)&&(iph->saddr!=netInfo.local_IP)) //if dest IP is not TunIP or src addr not local IP => need to relay
                    packetSend(rawFd,packet,dataLen,packetHeader);
            }

         //   relayFunc(rawFd,recvRawFd,recvPacketBuffer,sizeof(recvPacketBuffer),recvPacketLen,(uint8_t*)&netInfo);
        }


    pcap_close(pcd);
    close(rawFd);
    t1.join();
    return 0;
}

bool decapPacket(uint8_t **packet, int &dataLen,CNetInfo& netInfo)
{

    struct iphdr* iph=(struct iphdr*)(*packet);
    if(parseIP(packet,dataLen,IPPROTO_TCP))
    {

        struct tcphdr* tcph=(struct tcphdr*)(*packet);
        if(ntohs(tcph->dest)==4885&&iph->saddr!=netInfo.local_IP)
        {
            if(parseTCPData(packet,dataLen))
            {
                return true;
            }
        }
    }


    return false;
}

void forwardPacket(int rawFd,uint8_t** packet,int dataLen,CNetInfo& netInfo)
{
    if(decapPacket(packet,dataLen,netInfo))
    {
        struct iphdr* iph=(struct iphdr*)(*packet);
        packetSendTo(rawFd,*packet,dataLen,iph->protocol,netInfo.tun_IP);
    }
}


/**********************************************************Server Algorism******************************************************/
bool decapPacket(uint8_t **packet, int &dataLen,struct MapKey& mapKey,SNetInfo& netInfo,map<struct MapKey,uint32_t>& relayInfo,map<struct MapKey, uint32_t>::iterator& iter)
{
    if(parseEther(packet,dataLen,ETHERTYPE_IP))
    {
        struct iphdr* iph = (struct iphdr*)(*packet);
        if(parseIP(packet,dataLen,IPPROTO_TCP))
        {

            struct tcphdr* tcph = (struct tcphdr*)(*packet);  //need to add relayPacket logic (server - > proxy - > Android
            if(ntohs(tcph->dest)==4885) //forward packet
            {
                /**************************************origin Data Area***************************************************/
                if(parseTCPData(packet,dataLen)) //decapsulation packet(origin ip header point)
                {

                    /******************************** make key of Map & save Value(Src Addr)*******************************/
                    struct iphdr* originIph=(struct iphdr*)(*packet);
                    if(originIph->protocol==IPPROTO_TCP)
                    {
                        struct tcphdr* originTcph=(struct tcphdr*)(*packet + originIph->ihl*4);
                        if(originIph->saddr!=netInfo.local_IP&&originIph->daddr!=netInfo.local_IP)//if not relay packet => new session packet
                        {
                            mapKey.destIP=originIph->daddr;
                            mapKey.destPort=originTcph->dest;
                            mapKey.srcPort=originTcph->source;
                            mapKey.protocol=originIph->protocol;
                            mapKey.id=0;

                            netInfo.packetProtocol=originIph->protocol;
                            if(originTcph->rst!=1)
                                netInfo.sessionFlag=true;
                            if((iter=relayInfo.find(mapKey))!=relayInfo.end())
                            {
                                if(iter->second==iph->saddr) //already exist same key, value
                                    netInfo.makeMapFlag=false;
                                else{
                                    cout<<"origin value : "<<iter->second<<endl;
                                    cout<<"Captured value : "<<iph->saddr<<endl;
                                    netInfo.makeMapFlag=true; //have a same key but value is diffrent
                                    netInfo.mapValue=iph->saddr;          //save Src addr to make map
                                }
                            }else
                            {

                                netInfo.mapValue=iph->saddr;          //save Src addr to make map
                                netInfo.makeMapFlag=true;
                                cout<<"Port 4885 & TCP Packet Detected! Sesstion flag Set ! & mapValue = "<<netInfo.mapValue<<endl;

                            }
                        }

//                        if((originTcph->fin==1)||(originTcph->rst==1)) //if end Session
//                            netInfo.delSesstionFlag=true;

                    }else if(originIph->protocol==IPPROTO_UDP)
                    {
                        struct udphdr* originUdph=(struct udphdr*)(*packet+ originIph->ihl*4);
                        if(originIph->saddr!=netInfo.local_IP&&originIph->daddr!=netInfo.local_IP)//if not relay packet => new session packet
                        {
                            mapKey.destIP=originIph->daddr;
                            mapKey.destPort=originUdph->dest;
                            mapKey.srcPort=originUdph->source;
                            mapKey.protocol=originIph->protocol;
                            mapKey.id=0;

                            netInfo.packetProtocol=originIph->protocol;


                            netInfo.sessionFlag=true;
                            if((iter=relayInfo.find(mapKey))!=relayInfo.end())
                            {
                                if(iter->second==iph->saddr) //already exist same key, value
                                    netInfo.makeMapFlag=false;
                                else{
                                    cout<<"origin value : "<<iter->second<<endl;
                                    cout<<"Captured value : "<<iph->saddr<<endl;
                                    netInfo.makeMapFlag=true; //have a same key but value is diffrent
                                    netInfo.mapValue=iph->saddr;          //save Src addr to make map
                                }
                            }else
                            {
                                netInfo.mapValue=iph->saddr;          //save Src addr to make map
                                netInfo.makeMapFlag=true;
                                cout<<"Port 4885 & UDP Packet Detected! Sesstion flag Set ! & mapValue = "<<netInfo.mapValue<<endl;
                            }

                        }
                    }else if(originIph->protocol==IPPROTO_ICMP)
                    {
                        struct icmp* originIcmph = (struct icmp*)(*packet+originIph->ihl*4);
                        if(originIph->saddr!=netInfo.local_IP&&originIph->daddr!=netInfo.local_IP)//if not relay packet => new session packet
                        {
                            mapKey.destIP=originIph->daddr;
                            mapKey.destPort=0;
                            mapKey.srcPort=0;
                            mapKey.protocol=originIph->protocol;
                            mapKey.id=originIcmph->icmp_hun.ih_idseq.icd_id;         //icmp id value

                            netInfo.packetProtocol=originIph->protocol;


                            netInfo.sessionFlag=true;
                            if((iter=relayInfo.find(mapKey))!=relayInfo.end())
                            {

                                if(iter->second==iph->saddr) //already exist same key, value
                                    netInfo.makeMapFlag=false;
                                else{
                                    cout<<"origin value : "<<iter->second<<endl;
                                    cout<<"Captured value : "<<iph->saddr<<endl;
                                    netInfo.makeMapFlag=true; //have a same key but value is diffrent
                                    netInfo.mapValue=iph->saddr;          //save Src addr to make map
                                }
                            }else
                            {
                                netInfo.mapValue=iph->saddr;          //save Src addr to make map
                                netInfo.makeMapFlag=true;
                                cout<<"Port 4885 & ICMP Packet Detected! Sesstion flag Set ! & mapValue = "<<netInfo.mapValue<<endl;
                            }



                        }
                    }
                    return true;
                    /**************************************origin Data Area***************************************************/
                }

            }else{
                // if not 4885 port
                //check packet if packet is need to relay
                //not used id
                if(iph->saddr!=netInfo.local_IP) //if not send localhost or dest is not mine
                {
                    struct tcphdr* tcph=(struct tcphdr*)*packet;
                    struct MapKey checkKey; //id & port values init 0

                    checkKey.destIP=iph->saddr;
                    checkKey.destPort=tcph->source;
                    checkKey.srcPort=tcph->dest;
                    checkKey.protocol=iph->protocol;
                    checkKey.id=0;


                    if(relayInfo.find(checkKey)!=relayInfo.end()||tcph->rst!=1) //if need to relay & rst not set(operation permmit error)
                    {
                        netInfo.relayFlag=true;
                        netInfo.mapValue=relayInfo[checkKey];
//                        if((tcph->fin==1)||(tcph->rst==1)) //if need to relay & end Session
//                            netInfo.delSesstionFlag=true;
                    }else
                        netInfo.relayFlag=false;

                    return true;
                }
            }
        }else if(parseIP(packet,dataLen,IPPROTO_UDP)&&(iph->saddr!=netInfo.local_IP))
        {
            cout<<"Check UDP Packet"<<endl;
            //check packet if packet is need to relay
            //not used id
            struct udphdr* udph=(struct udphdr*)(*packet);
            struct MapKey checkKey;

            checkKey.destIP=iph->saddr;
            checkKey.destPort=udph->source;
            checkKey.srcPort=udph->dest;
            checkKey.protocol=iph->protocol;
            checkKey.id=0;


            if(relayInfo.find(checkKey)!=relayInfo.end()) //if need to relay
            {
                cout<<"Correct!"<<endl;
                netInfo.relayFlag=true;
                netInfo.mapValue=relayInfo[checkKey];
            }else{
                netInfo.relayFlag=false;
                cout<<"Incorrect!!"<<endl;
            }
           return true;
        }
        else if(parseIP(packet,dataLen,IPPROTO_ICMP)&&(iph->saddr!=netInfo.local_IP))
        {
            //check packet if packet is need to relay
            //not used ports
            struct icmp* icmph=(struct icmp*)(*packet);
            struct MapKey checkKey;

            checkKey.destIP=iph->saddr;
            checkKey.destPort=0;
            checkKey.srcPort=0;
            checkKey.protocol=iph->protocol;
            checkKey.id=icmph->icmp_hun.ih_idseq.icd_id;


            if(relayInfo.find(checkKey)!=relayInfo.end()) //if need to relay
            {
                netInfo.relayFlag=true;
                netInfo.mapValue=relayInfo[checkKey];

            }else
                netInfo.relayFlag=false;


            return true;
        }

    }
    return false; //nothing to do (packet is occured by Proxy)
}

bool packetRelay(int rawFd, uint8_t *packet, int& dataLen,uint32_t local_IP,struct MapKey& mapKey,map<struct MapKey, uint32_t>::iterator& iter,SNetInfo& netInfo,map<struct MapKey,uint32_t>& relayInfo,PacketHeader& packetHeader)
{
    uint8_t* packetPoint=packet+sizeof(struct ether_header);
    int packetLen=dataLen-sizeof(struct ether_header);


    /***************************************Android <--> proxy <--> server *******************************************************/
    if(!decapPacket(&packet,dataLen,mapKey,netInfo,relayInfo,iter)) //check packet if tunneling packet or need to relay packet
        return false;

    if(netInfo.sessionFlag&&!(netInfo.relayFlag))
    {

        if(netInfo.makeMapFlag)
        {
            cout<<"#############################MAKE MAP##################################"<<endl;

            relayInfo[mapKey]=netInfo.mapValue;         //Make new Map
            for (iter = relayInfo.begin(); iter != relayInfo.end(); ++iter)
                cout << "(" << iter->first << "," << (*iter).second << ")" << " ";
            cout << endl;

            cout<<"#############################MAKE MAP##################################"<<endl;
            cout<<endl;
            netInfo.makeMapFlag=false;
        }
        packetSend(rawFd,packet,dataLen,local_IP,netInfo.packetProtocol); //send packet & modify src IP is local IP
        netInfo.sessionFlag=false;
        return true;
    }

//    if(netInfo.delSesstionFlag&&!(netInfo.relayFlag))
//    {
//        cout<<"Come in"<<endl;
//        relayInfo.erase(mapKey); //reset map key & value
//        packetSend(rawFd,packet,dataLen,local_IP,netInfo.packetProtocol); //send packet & modify src IP is local IP
//        netInfo.delSesstionFlag=false;
//    }

    if(netInfo.relayFlag)
    {
        cout<<"Relay!!"<<endl;
        packetHeader.iph.daddr=netInfo.mapValue; //change to Destination address is mapValue(origin PhoneIP)

        packetSend(rawFd,packetPoint,packetLen,packetHeader);
        netInfo.relayFlag=false;
    }
    /***************************************Android --> proxy --> server *******************************************************/


    return true;

}

bool checkPacket(uint8_t **packet, int dataLen, uint32_t srcIP)
{
    uint8_t** originPacket=packet;
    if(parseEther(packet,dataLen,ETHERTYPE_IP))
    {
        struct iphdr* iph=(struct iphdr*)*packet;
        if(iph->saddr==srcIP) //if send localhost
            return false;

        /*need to delete*/
        /*
        if(parseIP(packet,dataLen,IPPROTO_TCP))
        {
            struct tcphdr* tcph=(struct tcphdr*)*packet;
            if(tcph->rst==1)
                return false;
        }
        */
    }


    packet=originPacket; //restore addr
    return true;
}

void relayFunc(int rawFd,int recvRawFd,uint8_t* recvPacketBuffer,int sizeOfBuf,int recvPacketLen,uint8_t* netInfoP) //netinfo need to call by pointer maybe Thread can't know CNetInfo class or can't Reference
{
    CNetInfo* netInfo=(CNetInfo*)netInfoP;

    while (rRecvPacket(recvRawFd,recvPacketBuffer,sizeOfBuf,recvPacketLen))
    {
        uint8_t* packet=recvPacketBuffer;
        int dataLen=recvPacketLen;

        if(checkPacket(&packet,dataLen,netInfo->local_IP))
            forwardPacket(rawFd,&packet,dataLen,*netInfo);
    }
}
