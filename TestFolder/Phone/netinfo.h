#ifndef NETINFO_H
#define NETINFO_H

/*Moblie NetInfo*/

#include <iostream>

#define IF_NAMESIZE 16
#define IP_SIZE 16


class CNetInfo
{
public:
    CNetInfo();
    char tunDev[IF_NAMESIZE];
    char localDev[IF_NAMESIZE];
    char gatewayDev[IF_NAMESIZE];
    char localIP[IP_SIZE];
    char proxyIP[IP_SIZE];
    char tunIP[IP_SIZE];
    char gatewayIP[IP_SIZE];
    char* proxyDomain=(char*)"hanbin.iptime.org";
    std::string executeRouteSet="./route_set.sh ";
    uint32_t local_IP;
    uint32_t proxy_IP;
    uint32_t tun_IP;
    uint32_t gateway_IP;
    uint32_t _network;
    uint32_t _netmask;
    void init();
    void printInfo();
    void getNetInfo();
    void getIpFromDomain();
    int getGatewayIface();
};

class SNetInfo
{

public:
    SNetInfo();
    char tunDev[IF_NAMESIZE];
    char localDev[IF_NAMESIZE];
    char gatewayDev[IF_NAMESIZE];
    char localIP[IP_SIZE];
    char phoneIP[IP_SIZE];
    char tunIP[IP_SIZE];
    char gatewayIP[IP_SIZE];


    uint32_t local_IP;
    uint32_t phone_IP;
    uint32_t tun_IP;
    uint32_t gateway_IP;
    uint32_t _network;
    uint32_t _netmask;
    uint32_t mapValue;
    uint8_t packetProtocol;
    bool sessionFlag=false;
    bool delSesstionFlag=false;
    bool relayFlag=false;
    bool makeMapFlag=false;
    void init();
    void printInfo();
    void getNetInfo();
    int getGatewayIface();
};

#endif // NETINFO_H
