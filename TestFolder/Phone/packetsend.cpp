#include "packetsend.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "errhandling.h"
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>

#include <iostream>
#include "printdata.h"
#include "calchecksum.h"
#include "jpcaplib.h"

using namespace std;

struct MaximumSegementSize{
    uint8_t kind;
    uint8_t length;
    uint16_t MSSvalue;
}typedef MaximumSegementSize;

void packetSend(int fd,uint8_t *data, int dataLen,PacketHeader& packetHeader)
{

    int packetLen=dataLen+sizeof(PacketHeader);
    uint8_t packet[packetLen]; //need to modify

    //packetHeader.incId();
    packetHeader.iph.tot_len=htons(packetLen); //set total len
    //packetHeader.tcph.doff=htons(sizeof(struct tcphdr))/4;



    /* merge header + data*/
    memcpy(packet,&packetHeader,sizeof(packetHeader));
    memcpy(packet+sizeof(PacketHeader),data,dataLen);


    /*calculation checksum*/
    calTCPChecksum(packet,packetLen);
    calIPChecksum(packet);

    /*set dstAddr*/
    struct sockaddr_in dstAddr;
    dstAddr.sin_family=AF_INET;
    dstAddr.sin_addr.s_addr=packetHeader.iph.daddr;
    dstAddr.sin_port=packetHeader.tcph.dest;

//    printByHexData(packet,packetLen);


    if(sendto(fd,packet,packetLen,MSG_EOR,(struct sockaddr *)&dstAddr,(socklen_t)sizeof(dstAddr))<0)
    {
        cout<<"Come Packet Send 1 Info "<<endl;
        printByHexData(data,dataLen);
        cout<<"DataLen  : "<<dataLen;
        printByHexData(packet,packetLen);
        cout<<"Packet Len : "<<packetLen<<endl;
        errorRetn("Sendto error in packetSend1");
    }

 //   packetHeader.incId(); //inc Id
  //  packetHeader.incSeq();


}

bool packetSend(int rawFd, uint8_t *packet, int packetLen, uint32_t local_IP,uint8_t protocol)
{

    struct iphdr* iph=(struct iphdr*)packet;

    //modify source addr to my IP
    iph->saddr=local_IP;

    /*calculation checksum*/

    if(protocol==IPPROTO_TCP)
        calTCPChecksum(packet,packetLen);
    else if(protocol==IPPROTO_ICMP)
        calICMPChecksum(packet,packetLen);
    else if(protocol==IPPROTO_UDP)
        calUDPChecksum(packet,packetLen);

    calIPChecksum(packet);


    /*set dstAddr*/
    struct sockaddr_in dstAddr;
    dstAddr.sin_family=AF_INET;

    dstAddr.sin_addr.s_addr=iph->daddr;

    if(iph->protocol==IPPROTO_TCP)
    {
        struct tcphdr* tcph=(struct tcphdr*)packet+((iph->ihl*4));
//        if(tcph->rst==1)
//            return false;
        dstAddr.sin_port=tcph->dest;
    }else if(iph->protocol==IPPROTO_UDP)
    {
        struct udphdr* udph=(struct udphdr*)packet+((iph->ihl*4));
        dstAddr.sin_port=udph->dest;
    }
    else{
        dstAddr.sin_port=0;
    }
    if(sendto(rawFd,packet,packetLen,MSG_EOR,(struct sockaddr *)&dstAddr,(socklen_t)sizeof(dstAddr))<0)
    {
        printByHexData(packet,packetLen);
        cout<<"Packet Len : "<<packetLen<<endl;
        errorRetn("Sendto error in packetSend2");
    }

    return true;
}

bool packetSendTo(int fd, uint8_t*packet, int packetLen, uint8_t protocol,uint32_t dest_IP)
{
    struct iphdr* iph=(struct iphdr*)packet;

    //modify Dest addr to dest_IP
    iph->daddr=dest_IP;

  //  packetLen=ntohs(iph->tot_len);
    /*calculation checksum*/
    if(protocol==IPPROTO_TCP)
        calTCPChecksum(packet,packetLen);
    else if(protocol==IPPROTO_ICMP)
        calICMPChecksum(packet,packetLen);
    else if(protocol==IPPROTO_UDP)
        calUDPChecksum(packet,packetLen);

    calIPChecksum(packet);



    /*set dstAddr*/
    struct sockaddr_in dstAddr;
    dstAddr.sin_family=AF_INET;

    dstAddr.sin_addr.s_addr=iph->daddr;

    if(iph->protocol==IPPROTO_TCP)
    {
        struct tcphdr* tcph=(struct tcphdr*)packet+((iph->ihl*4));
//        if(tcph->rst==1)
//            return false;
        dstAddr.sin_port=tcph->dest;
    }else if(iph->protocol==IPPROTO_UDP)
    {
        struct udphdr* udph=(struct udphdr*)packet+((iph->ihl*4));
        dstAddr.sin_port=udph->dest;
    }
    else{
        dstAddr.sin_port=0;
    }
    if(sendto(fd,packet,packetLen,MSG_EOR,(struct sockaddr *)&dstAddr,(socklen_t)sizeof(dstAddr))<0)
    {
        printByHexData(packet,packetLen);
        cout<<"Packet Len : "<<packetLen<<endl;
        errorRetn("Sendto error in packetSend3");
    }

    return true;

}
