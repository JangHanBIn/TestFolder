#ifndef PACKETSEND_H
#define PACKETSEND_H
#include <iostream>
#include "PacketHeader.h"


void packetSend(int fd, uint8_t* data, int dataLen, PacketHeader &packetHeader); //Userdata + origin packetSend Method
bool packetSend(int rawFd, uint8_t* packet, int packetLen, uint32_t local_IP, uint8_t protocol); // send to Server & modify Src IP
bool packetSendTo(int fd, uint8_t *packet, int packetLen, uint8_t protocol, uint32_t dest_IP);
#endif // PACKETSEND_H
