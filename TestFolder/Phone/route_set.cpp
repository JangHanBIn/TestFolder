#include "route_set.h"
#include <arpa/inet.h>
#include <net/route.h>
#include "errhandling.h"
#include <fcntl.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <iostream>

using namespace std;

void gateway_set(char *addr)
{


      int sockfd;
      struct rtentry route;
      struct sockaddr_in *address_in;
      int err = 0;

      // create the socket
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0))<0)
        errorRetn("Socket Open error in gateway_set");

      memset(&route, 0, sizeof(route));
      address_in = (struct sockaddr_in*) &route.rt_gateway;
      address_in->sin_family = AF_INET;
      address_in->sin_addr.s_addr = inet_addr(addr);
      address_in = (struct sockaddr_in*) &route.rt_dst;
      address_in->sin_family = AF_INET;
      address_in->sin_addr.s_addr = INADDR_ANY;
      address_in = (struct sockaddr_in*) &route.rt_genmask;
      address_in->sin_family = AF_INET;
      address_in->sin_addr.s_addr = INADDR_ANY;
      // TODO Add the interface name to the request
      route.rt_flags = RTF_UP | RTF_GATEWAY;
      route.rt_metric = 0;
      if ((err = ioctl(sockfd, SIOCADDRT, &route)) != 0)
          errorRetn("SIOCADDRT error in gateway_set");
}

void routing_set(char *gateway,uint32_t network,uint32_t netmask)
{


      int sockfd;
      struct rtentry route;
      struct sockaddr_in *address_in;
      int err = 0;

      // create the socket
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0))<0)
        errorRetn("Socket Open error in gateway_set");


      memset(&route, 0, sizeof(route));
      address_in = (struct sockaddr_in*) &route.rt_gateway;
      address_in->sin_family = AF_INET;
      address_in->sin_addr.s_addr = inet_addr(gateway);
      address_in = (struct sockaddr_in*) &route.rt_dst;
      address_in->sin_family = AF_INET;
      address_in->sin_addr.s_addr = network;
      //cout<<inet_ntoa(*(in_addr*)&address_in->sin_addr.s_addr)<<endl;
      address_in = (struct sockaddr_in*) &route.rt_genmask;
      address_in->sin_family = AF_INET;
      address_in->sin_addr.s_addr = netmask;
      // TODO Add the interface name to the request
      route.rt_flags = RTF_UP | RTF_GATEWAY;
      route.rt_metric = 0;
      if ((err = ioctl(sockfd, SIOCADDRT, &route)) != 0)
          errorRetn("SIOCADDRT error in routing_set");
}


void ProxyRouting_set(uint32_t gateway, uint32_t destination, uint32_t netmask,CNetInfo netInfo)
{


      int sockfd;
      struct rtentry route;
      struct sockaddr_in *address_in;
      int err = 0;

      // create the socket
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0))<0)
        errorRetn("Socket Open error in gateway_set");


      memset(&route, 0, sizeof(route));
      address_in = (struct sockaddr_in*) &route.rt_gateway;
      address_in->sin_family = AF_INET;
      address_in->sin_addr.s_addr = gateway;
      address_in = (struct sockaddr_in*) &route.rt_dst;
      address_in->sin_family = AF_INET;
      address_in->sin_addr.s_addr = destination;
      //cout<<inet_ntoa(*(in_addr*)&address_in->sin_addr.s_addr)<<endl;
      address_in = (struct sockaddr_in*) &route.rt_genmask;
      address_in->sin_family = AF_INET;
      address_in->sin_addr.s_addr = netmask;
      // TODO Add the interface name to the request
      route.rt_dev=netInfo.localDev;
      route.rt_flags = RTF_UP | RTF_GATEWAY;
      route.rt_metric = 0;
      if ((err = ioctl(sockfd, SIOCADDRT, &route)) != 0)
            perror("need to delete Routing table After Process");
}

static void ProxyRouting_del(uint32_t gateway, uint32_t destination, uint32_t netmask, CNetInfo netInfo)
{

      int sockfd;
      struct rtentry route;
      struct sockaddr_in *address_in;
      int err = 0;

      // create the socket
    if((sockfd = socket(AF_INET, SOCK_DGRAM, 0))<0)
        errorRetn("Socket Open error in gateway_set");


      memset(&route, 0, sizeof(route));
      address_in = (struct sockaddr_in*) &route.rt_gateway;
      address_in->sin_family = AF_INET;
      address_in->sin_addr.s_addr = gateway;
      address_in = (struct sockaddr_in*) &route.rt_dst;
      address_in->sin_family = AF_INET;
      address_in->sin_addr.s_addr = destination;
      //cout<<inet_ntoa(*(in_addr*)&address_in->sin_addr.s_addr)<<endl;
      address_in = (struct sockaddr_in*) &route.rt_genmask;
      address_in->sin_family = AF_INET;
      address_in->sin_addr.s_addr = netmask;
      // TODO Add the interface name to the request
      route.rt_dev=netInfo.localDev;
      route.rt_flags = RTF_UP | RTF_GATEWAY;

      if ((err = ioctl(sockfd, SIOCDELRT, &route)) != 0)
            perror(" delete Routing table Fail");

}

void serverRoute_set()
{

}
