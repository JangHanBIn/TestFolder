#ifndef ROUTESET_H
#define ROUTESET_H

#include <iostream>
#include "netinfo.h"

void gateway_set(char* addr);
void routing_set(char *gateway, uint32_t network, uint32_t netmask);
void ProxyRouting_set(uint32_t gateway, uint32_t destination, uint32_t netmask,CNetInfo netInfo);
void serverRoute_set();
//static void ProxyRouting_del(uint32_t gateway, uint32_t destination, uint32_t netmask, CNetInfo netInfo);
#endif // ROUTESET_H
