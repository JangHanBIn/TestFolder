ifconfig tun0 mtu 1350
ip route add default via 10.1.1.128 dev tun0 table 4885
ip route add "$1"/32 via "$2" dev "$3" table 4885
iptables -A OUTPUT -p tcp --tcp-flags RST RST --dport 4885 -j DROP
iptables -A OUTPUT -p tcp --tcp-flags RST RST --sport 4885 -j DROP
ip rule add from all table 4885 priority 0
ip route flush cache
