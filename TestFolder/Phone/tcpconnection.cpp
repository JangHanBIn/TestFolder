#include "tcpconnection.h"
#include "packetsend.h"
#include "printdata.h"
#include <unistd.h>
#include "jrawlib.h"

#define SET 1

bool connectServer(int& rawFd,CNetInfo &netInfo,PacketHeader& packetHeader)
{
    //Set packet header
    packetHeader.init(netInfo.local_IP,netInfo.proxy_IP,4885,4885);//set Src IP Dest IP , Src port Dest Port
  //  packetHeader.makeId();
    packetHeader.makeSeq();

    cout<<"Send Syn Packet"<<endl;
    sendSyn(rawFd,packetHeader);                    //call by value


    int recvRawFd=rOpen(IPPROTO_TCP);

    if(recvSynAck(recvRawFd,packetHeader))                //need to save Ack
    {
        cout<<"Recived Syn Ack Packet"<<endl;
    //    packetHeader.incId();
        cout<<"Send Ack Packet"<<endl;
        sendAck(rawFd,packetHeader);

    }

    packetHeader.tcph.doff=5;
    packetHeader.rstFlags();
    packetHeader.tcph.psh=SET;
    packetHeader.tcph.ack=SET;
    cout<<"TCP Connection to server Done"<<endl;
    close(recvRawFd);
  //  packetHeader.incSeq();

    return true;
}

void sendSyn(int fd, PacketHeader& packetHeader)
{


    packetHeader.rstFlags();
    //set flag to syn
    packetHeader.tcph.syn=SET;

    SynOptions synOptions;

    /*set MaximumSegementSize*/
    synOptions.maximumSegementSize.kind=2; //Maximum Segment Size
    synOptions.maximumSegementSize.length=4;
    synOptions.maximumSegementSize.MSSvalue=ntohs(1420); //set MSS value 1420

    /*set TCP SACK Permitted option*/
    synOptions.tcpSackPermittedOption.kind=4; //SACK Permitted
    synOptions.tcpSackPermittedOption.length=2;

    /* set Time Stamps*/
    synOptions.timestamp.kind=8; //Time Stamp Option
    synOptions.timestamp.length=10;
    synOptions.timestamp.timeStampValue=0;
    synOptions.timestamp.timeStampEchoReply=0;

    //No-Operation is already set

    /* set Window Scale*/
    synOptions.windowScale.kind=3; //Window Scale
    synOptions.windowScale.length=3;
    synOptions.windowScale.shifCount=7;

    /*set tcp configuration*/
    packetHeader.tcph.doff=(sizeof(packetHeader.tcph)+sizeof(SynOptions))/4;



    packetSend(fd,(uint8_t*)&synOptions,sizeof(SynOptions),packetHeader);



}

bool recvSynAck(int rawFd, PacketHeader &packetHeader)
{
    //need to get rid of cost by init Parents method
    uint8_t recvPacketBuffer[65536];
    memset(recvPacketBuffer,0,sizeof(recvPacketBuffer));
    int recvPacketLen=0;

    while(rRecvPacket(rawFd,recvPacketBuffer,sizeof(recvPacketBuffer),recvPacketLen))
    {
        uint8_t* packet=recvPacketBuffer;
        if(parseIP(&packet,recvPacketLen,IPPROTO_TCP))
        {

            struct tcphdr* tcph = (struct tcphdr*)packet;
            if((tcph->syn==SET&&tcph->ack==SET&&(ntohs(tcph->dest)==4885)))
            {
                //set Ack Num
                packetHeader.tcph.ack_seq=tcph->seq;            //save Server Seq num -> Client Ack
                packetHeader.incAck();
                return true;
            }

        }
    }
    return false;
}

void sendAck(int fd, PacketHeader& packetHeader)
{
    //packetHeader.tcph.window=htons(128);
    packetHeader.incSeq();
    packetHeader.rstFlags();
    packetHeader.tcph.ack=SET;

    AckOptions ackOptions;
    ackOptions.timeStamps.kind=8;
    ackOptions.timeStamps.length=10;
    ackOptions.timeStamps.timeStampEchoReply=0;
    ackOptions.timeStamps.timeStampValue=0;


    /*set tcp configuration*/
    packetHeader.tcph.doff=(sizeof(packetHeader.tcph)+sizeof(AckOptions))/4;



    packetSend(fd,(uint8_t*)&ackOptions,sizeof(AckOptions),packetHeader);

}


bool connectClient(pcap_t *pcd, int &rawFd, SNetInfo &netInfo, PacketHeader &packetHeader)
{
 //   packetHeader.makeId();
    packetHeader.makeSeq();
    packetHeader.iph.saddr=netInfo.local_IP;
    cout<<"Waiting... to Client Syn"<<endl;
    if(recvSyn(pcd,packetHeader))              //need to save Client IP , Port Seq num
    {
        cout<<"Recived Syn Packet"<<endl;
        sendSynAck(rawFd,packetHeader);
        cout<<"Send Syn Ack Packet"<<endl;

        while(!recvAck(pcd,packetHeader))
            cout<<"Recived Ack Packet"<<endl;
    }

    netInfo.phone_IP=packetHeader.iph.daddr;
    netInfo.init();
    packetHeader.tcph.doff=5;
//    packetHeader.incSeq();
    cout<<"TCP Connetion & init netInfo done"<<endl;

    return true;
}

bool recvSyn(pcap_t *pcd, PacketHeader &packetHeader)
{
    uint8_t* packet;
    int dataLen=0;
    while (true)
    {
        if(recvPacket(pcd,&packet,dataLen))
            if(parseEther(&packet,dataLen,ETHERTYPE_IP))
            {
//                cout<<"IP Header"<<endl;
//                printByHexData(packet,dataLen);
                uint8_t* ip=packet;
                if(parseIP(&packet,dataLen,IPPROTO_TCP))
                {

//                    cout<<"TCP Header"<<endl;
//                    printByHexData(packet,dataLen);

                    struct tcphdr* tcph = (struct tcphdr*)packet;
                    if((tcph->syn==SET&&(ntohs(tcph->dest)==4885)))
                    {
                        //set IP , Port , Seq num
                        //add exchange method (IP , TCP)
                        struct iphdr* iph=(struct iphdr*)ip;
                        packetHeader.iph.saddr=iph->daddr;
                        packetHeader.iph.daddr=iph->saddr;

                        packetHeader.tcph.source=tcph->dest;
                        packetHeader.tcph.dest=tcph->source;

                        packetHeader.tcph.ack_seq=tcph->seq;
                        packetHeader.incAck();

                        return true;
                    }

                }

            }
    }

    return false;

}

void sendSynAck(int fd, PacketHeader& packetHeader)
{
    //packetHeader.rstFlags();
    packetHeader.tcph.syn=SET;
    packetHeader.tcph.ack=SET;

    SynAckOptions synAckOptions;

    /*set MaximumSegementSize*/
    synAckOptions.maximumSegementSize.kind=2; //Maximum Segment Size
    synAckOptions.maximumSegementSize.length=4;
    synAckOptions.maximumSegementSize.MSSvalue=ntohs(1420); //set MSS value 1420

    /*set TCP SACK Permitted option*/
    synAckOptions.tcpSackPermittedOption.kind=4; //SACK Permitted
    synAckOptions.tcpSackPermittedOption.length=2;


    /* set Time Stamps*/
    synAckOptions.timestamp.kind=8; //Time Stamp Option
    synAckOptions.timestamp.length=10;
    synAckOptions.timestamp.timeStampValue=0;
    synAckOptions.timestamp.timeStampEchoReply=0;


    /* set Window Scale*/
    synAckOptions.windowScale.kind=3; //Window Scale
    synAckOptions.windowScale.length=3;
    synAckOptions.windowScale.shifCount=7;

    /*set tcp configuration*/
    packetHeader.tcph.doff=(sizeof(packetHeader.tcph)+sizeof(SynAckOptions))/4;


    packetSend(fd,(uint8_t*)&synAckOptions,sizeof(SynAckOptions),packetHeader);
}

bool recvAck(pcap_t *pcd, PacketHeader &packetHeader)
{
    uint8_t* packet;
    int dataLen=0;
    while (true)
    {
        if(recvPacket(pcd,&packet,dataLen))
            if(parseEther(&packet,dataLen,ETHERTYPE_IP))
            {
                if(parseIP(&packet,dataLen,IPPROTO_TCP))
                {

                    struct tcphdr* tcph = (struct tcphdr*)packet;
                    if((tcph->ack==SET&&(ntohs(tcph->dest)==4885)))
                    {
                        packetHeader.incSeq();
//                        packetHeader.rstFlags();
                        packetHeader.tcph.syn=0;
                        packetHeader.tcph.psh=SET;
//                        packetHeader.tcph.ack=SET;
                      //  packetHeader.tcph.seq=tcph->ack_seq; // Server Seq num add 1
                        return true;
                    }

                }

            }
    }

    return false;
}


