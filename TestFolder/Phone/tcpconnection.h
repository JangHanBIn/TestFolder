#ifndef TCPCONNECTION_H
#define TCPCONNECTION_H

#include "jpcaplib.h"
#include "PacketHeader.h"
#include "netinfo.h"

#pragma pack(push,1)

struct MaximumSegementSize{
    uint8_t kind;
    uint8_t length;
    uint16_t MSSvalue;
}typedef MaximumSegementSize;

struct TcpSackPermittedOption{
    uint8_t kind;
    uint8_t length;

}typedef TcpSackPermittedOption;

struct TimeStamps{
    uint8_t kind;
    uint8_t length;
    uint32_t timeStampValue;
    uint32_t timeStampEchoReply;

}typedef TimeStamps;

struct WindowScale{
    uint8_t kind;
    uint8_t length;
    uint8_t shifCount;
}typedef WindowScale;

struct SynOptions{

    /*Maximum segment Size*/
    MaximumSegementSize maximumSegementSize;

    /*TcpSackPermittedOption*/
    TcpSackPermittedOption tcpSackPermittedOption;

    /*TimeStamps*/
    TimeStamps timestamp;

    /*No-Operation*/
    uint8_t noOperation=1;

    /*Window Scale*/
    WindowScale windowScale;
}typedef SynOptions,SynAckOptions;

struct AckOptions{
    uint8_t Nop1=1;
    uint8_t Nop2=1;
    TimeStamps timeStamps;

};

#pragma pack(pop)

bool connectServer(int &rawFd, CNetInfo& netInfo, PacketHeader &packetHeader);
void sendSyn(int fd, PacketHeader &packetHeader);
bool recvSynAck(int rawFd, PacketHeader& packetHeader);
void sendAck(int fd, PacketHeader &packetHeader);
bool connectClient(pcap_t* pcd, int &rawFd, SNetInfo &netInfo, PacketHeader &packetheader);
bool recvSyn(pcap_t* pcd, PacketHeader& packetHeader);
void sendSynAck(int fd, PacketHeader &packetHeader);
bool recvAck(pcap_t* pcd, PacketHeader& packetHeader);
#endif // TCPCONNECTION_H
