#include "tuntap.h"
#include <fcntl.h>
#include <net/if.h>
#include <cstring>
#include <sys/ioctl.h>
#include <unistd.h>
#include <iostream>
#include <cstring>
#include <arpa/inet.h>
#include "errhandling.h"
#include <netdb.h>


using namespace std;

int tun_alloc(char *dev, int flags)
{
    struct ifreq ifr;
    char* clonedev=(char*)"/dev/net/tun";
    int fd;

    //open clonedev to able write , read
    if((fd=open(clonedev,O_RDWR))<0)
    {
        perror("tun_alloc fd open error!");
        exit(1);
        //return fd; //null return
    }

    //init ifr
    memset(&ifr,0,sizeof(ifr));

    //Set Flags

    ifr.ifr_ifru.ifru_flags=flags;
    /* Flags: IFF_TUN   - TUN device (no Ethernet headers)
     *        IFF_TAP   - TAP device
     *
     *        IFF_NO_PI - Do not provide packet information
     */




    /*if a device name was specified put it in the sturcture*/
    if(*dev)
        strncpy(ifr.ifr_ifrn.ifrn_name,dev,IF_NAMESIZE);
    /*if a device name was specified put it in the sturcture*/



    int err;
    if((err=ioctl(fd,TUNSETIFF,(void*)&ifr))<0)
    {
        perror("ioctl error!!!");
        close(fd);
        return err;
    }

    /* if the operation was successful, write back the name of the
     * interface to the variable "dev", so the caller can know
     * it. Note that the caller MUST reserve space in *dev (see calling
     * code below) */
    strcpy(dev, ifr.ifr_name);

    return fd;
}


void make_tun_dir()
{
    system("mkdir /dev/net/tun");
    system("mknod /dev/net/tun c 10 200");
    system("chmod 0666 /dev/net/tun");
}

int tun_set_queue(int fd, bool enable)
{
    struct ifreq ifr;

    memset(&ifr,0,sizeof(ifr));

    if(enable)
        ifr.ifr_flags = IFF_ATTACH_QUEUE;
    else
        ifr.ifr_flags = IFF_DETACH_QUEUE;

    return ioctl(fd,TUNSETQUEUE, (void *)&ifr);
}


int tun_set(char* dev, char *addr)
{
    struct ifreq ifr;
    struct sockaddr_in ipAddr;

    strncpy(ifr.ifr_name,dev,IF_NAMESIZE); //set interface Name
    memset(&ipAddr,0,sizeof(struct sockaddr)); //init ipAddr


    ipAddr.sin_family=AF_INET;
    ipAddr.sin_addr.s_addr=inet_addr(addr); //convert Dotted Decimal Notation to network byte
    ipAddr.sin_port=0;

    memcpy(&ifr.ifr_addr,&ipAddr,sizeof(struct sockaddr));
    int sockFd = socket(AF_INET,SOCK_DGRAM,0);


    if(ioctl(sockFd,SIOCSIFADDR,&ifr)<0)
        errorRetn("Set IP address error!");


    if(ioctl(sockFd,SIOCGIFFLAGS,&ifr)< 0)
        errorRetn("Set Flages error!");

    ifr.ifr_flags|=IFF_UP|IFF_RUNNING;

    if(ioctl(sockFd,SIOCSIFFLAGS,&ifr)< 0) //UP & running
        errorRetn("Set Flages error!2");


    return 0;
}


char* getLocalIP(char* device)
{
    int sockFd;
    struct ifreq ifr;

    sockFd=socket(AF_INET,SOCK_DGRAM,0);

    ifr.ifr_addr.sa_family=AF_INET;
    strcpy(ifr.ifr_name,device);
    if(ioctl(sockFd,SIOCGIFADDR,&ifr)<0)
        errorRetn("getLocalIP error in tuntap");



    return inet_ntoa(((struct sockaddr_in*)(&ifr.ifr_addr))->sin_addr);

}
