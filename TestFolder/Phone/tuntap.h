#ifndef TUNTAP_H
#define TUNTAP_H

#include <linux/if_tun.h>


void make_tun_dir();
int tun_alloc(char* dev,int flags);
int tun_set_queue(int fd, bool enable);
int tun_set(char* dev,char* addr);
char *getLocalIP(char *device);

#endif // TUNTAP_H

